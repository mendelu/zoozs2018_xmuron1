//
// Created by xproch17 on 30.11.2018.
//

#include "Hrdina.h"

Hrdina::Hrdina(std::string jmeno, std::string povolani,
int sila, int obrana){
    m_jmeno = jmeno;
    m_obrana = obrana;
    m_sila = sila;
    m_nazevPovolani = povolani;
}

void Hrdina::pridejZbran(Zbran* zbran){
    m_zbrane.push_back(zbran);
}

void Hrdina::pridejLektvar(Lektvar* lektvar){
    m_lektvary.push_back(lektvar);
}

int Hrdina::getObrana(){
    return m_obrana;
}

int Hrdina::getUtok(){
    int utokZbrani = 0;
    for (Zbran* zbran : m_zbrane){
        utokZbrani += zbran->getBonusSily();
    }
    return m_sila + utokZbrani;
}

void Hrdina::vypijHorniLektvar(){
    // TODO
}

void Hrdina::printInfo(){
    // TODO
}