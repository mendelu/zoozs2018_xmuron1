//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_HRDINADIRECTOR_H
#define CV10_HRDINADIRECTOR_H

#include "HrdinaBuilder.h"

class HrdinaDirector {
    HrdinaBuilder* m_builder;
public:
    HrdinaDirector(HrdinaBuilder* builder);
    void setBuilder(HrdinaBuilder* builder);
    Hrdina* createHrdina();
};


#endif //CV10_HRDINADIRECTOR_H
