//
// Created by xproch17 on 30.11.2018.
//
#include "Zbran.h"

Zbran::Zbran(std::string jmeno, int bonusSily){
    m_jmeno = jmeno;
    m_bonusSily = bonusSily;
}

int Zbran::getBonusSily(){
    return m_bonusSily;
}

std::string Zbran::getJmeno(){
    return m_jmeno;
}