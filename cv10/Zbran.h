//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_ZBRAN_H
#define CV10_ZBRAN_H

#include <iostream>

class Zbran {
    std::string m_jmeno;
    int m_bonusSily;
public:
    Zbran(std::string jmeno, int bonusSily);
    int getBonusSily();
    std::string getJmeno();
};


#endif //CV10_ZBRAN_H
