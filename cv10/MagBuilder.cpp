//
// Created by xproch17 on 30.11.2018.
//

#include "MagBuilder.h"

MagBuilder::MagBuilder(){

}

void MagBuilder::createNewHrdina(){
    m_hrdina = new Hrdina("Saladin", "Mag", 10, 15);
}

void MagBuilder::generujLektvary(){
    Lektvar* lektvarObrany = new Lektvar(0, 20);
    Lektvar* redBull = new Lektvar(20, 20);
    Lektvar* vodka = new Lektvar(-10, -10);
    Lektvar* lektvarSily = new Lektvar(20, -5);

    m_hrdina->pridejLektvar(lektvarObrany);
    m_hrdina->pridejLektvar(redBull);
    m_hrdina->pridejLektvar(vodka);
    m_hrdina->pridejLektvar(lektvarSily);
}

void MagBuilder::generujZbrane(){
    Zbran* hulka = new Zbran("hulka",5);
    m_hrdina->pridejZbran(hulka);
}