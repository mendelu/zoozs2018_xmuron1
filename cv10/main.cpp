#include <iostream>
#include "HrdinaDirector.h"
#include "MagBuilder.h"

int main() {
    HrdinaDirector* director
    = new HrdinaDirector(new MagBuilder);
    Hrdina* mag = director->createHrdina();
    /*
    director->setBuilder(new BarbarBuilder);
    Hrdina* b1 = director->createHrdina();
    Hrdina* b2 = director->createHrdina();
    */
    return 0;
}