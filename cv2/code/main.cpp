#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    string m_rc;
    string m_bydliste;

    void printInfo(){
        cout << "-Student-" << endl;
        cout << "Jmeno: " << m_jmeno << endl;
        cout << "RC: " << m_rc << endl;
        cout << "Bydliste: " << m_bydliste << endl;
    }

    void setBydliste(string noveBydliste){
        m_bydliste = noveBydliste;
    }

    Student(string jmeno, string rc){
        m_jmeno = jmeno;
        m_rc = rc;
        m_bydliste = "Nezadano";
    }

    Student(string jmeno, string rc, string bydliste) {
        m_jmeno = jmeno;
        m_rc = rc;
        m_bydliste = bydliste;
    }
};

int main() {
    Student* pepa = new Student("Pepa Novak","1151616/5150","Brno");
    pepa->printInfo();
    pepa->setBydliste("Praha");
    pepa->printInfo();
    return 0;
}