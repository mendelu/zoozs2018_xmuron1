#include <iostream>
#include <array>

using namespace std;


class Automobil {
    string m_model;
public:
    Automobil(string model){
       m_model = model;
    }

    string getModel(){
        return m_model;
    }
};

int main() {
    // Vytvorime instance
    Automobil* a1 = new Automobil("Skoda 1");
    Automobil* a2 = new Automobil("Skoda 2");
    Automobil* a3 = new Automobil("Skoda 3");

    // vytvorim array
    array<Automobil*, 3> garaz;

    // naplnim
    garaz[0] = a1;
    garaz[1] = a2;
    garaz[2] = a3;

    // vypisu
    cout << garaz.at(0)->getModel() << endl;

    // pruchod pomoci for cyklu
    for(Automobil* automobil: garaz){
        cout << automobil->getModel() << endl;
    }

    delete a1;
    delete a2;
    delete a3;

    return 0;
}