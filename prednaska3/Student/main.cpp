#include <iostream>
using namespace std;

class Student{

private:

    string m_jmeno;
    int m_semestr;
    float m_prumer;
    int m_vyplaceno;

    int spocitejNarok() {
        return (1000 * m_prumer) + (m_semestr * 100);
    }

    void setPrumer(float prumer){
        if(prumer >= 1.0 && prumer <= 5.0){
            m_prumer = prumer;
        }
        else{
            cout << "Zadana neplatna hodnota prumeru. Nastavuji defautlni hodnotu" << endl;
            m_prumer = 2.5;
        }
    }

public:
    Student(string jmeno, int semestr, float prumer){
        m_jmeno = jmeno;
        m_semestr = semestr;
        setPrumer(prumer);
        m_vyplaceno = 0;
    }

    void vypisStipendia(){
        cout << "Soucet vsech stipendii: " << m_vyplaceno << endl;
        cout << "Narok na stipendium: " << spocitejNarok() << endl;
    }

  void prictiStipendia(){
        m_vyplaceno += spocitejNarok();
        //m_vyplaceno = m_vyplaceno + spocitejNarok();
    }


};

int main() {
    Student* s = new Student("Karel",1,7.0);
    s->vypisStipendia();
    s->prictiStipendia();
    s->vypisStipendia();
    return 0;
}