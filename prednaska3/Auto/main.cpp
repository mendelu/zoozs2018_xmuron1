#include <iostream>
using namespace std;

class Auto{
private:
    int m_najetoKm;
    int m_cena;
    int m_zisk;

    int getCena(){
        int novaCena = m_cena;
        int pocetSlev = (m_najetoKm / 10000);
        for(int i=0;i<pocetSlev;i++){
            novaCena = novaCena * 0.9;
        }
        return novaCena;
    }

    void setZisk(int zisk){
        if(zisk > 0){
            m_zisk = zisk;
        }
    }

public:
    Auto(int najetoKm, int cena, int zisk){
        setZisk(zisk);
        m_najetoKm = najetoKm;
        m_cena = cena;
    }

    void pridatZaznam(int pocetDni, int najetoKm){
        m_najetoKm += najetoKm;
        m_zisk += cenaZaPujceni(pocetDni);
    }

    int cenaZaPujceni(int pocetDni){
        return getCena() * pocetDni;
    }


};

int main() {
    Auto* auto1 = new Auto(1000,100,0);
    cout << auto1->cenaZaPujceni(1) << endl;
    auto1->pridatZaznam(4,10000);
    cout << auto1->cenaZaPujceni(1) << endl;
    return 0;
}