#include <iostream>
using namespace std;

class Hrdina{
private:
    int m_sila;
    int m_obratnost;

public:
    Hrdina(int n){
        m_sila = n;
        m_obratnost = n;
    }

    void printInfo(){
        cout << m_sila << endl;
        cout << m_obratnost << endl;
    }

    void zvysUrovenSily(){
        m_sila = m_sila + 1;
        m_obratnost = m_obratnost - 1;
    }
};


int main() {
    Hrdina* h = new Hrdina(10);
    h->printInfo();
    h->zvysUrovenSily();
    h->printInfo();
    return 0;
}