#include <iostream>
using namespace std;

class Auto{
private:
    float m_najeto;
    float m_cena;
    float m_zisk;
    string m_posledniMajitel;

    float getCena(){
        float novaCena = m_cena;
        int pocetSlev = m_najeto / 10000;

        // sniznim o 10% tolikat kolik je pocetSlev
        for(int i=0;i<pocetSlev;i++) {
            novaCena = novaCena * 0.9;
        }

        return novaCena;
    }

    void setJmeno(string jmeno){
        if(jmeno.size() > 2){
            m_posledniMajitel = jmeno;
        }
        else{
            m_posledniMajitel = "Neplatny udaj";
            cout << "Zadane jmeno neni platne" << endl;
        }
    }
public:
    Auto(float najeto, float cena, float zisk, string jmeno){
        m_najeto = najeto;
        m_cena = cena;
        m_zisk = zisk;
        setJmeno(jmeno);
    }

    float spocitejCenu(int kolikDni){
        return getCena() * kolikDni;
    }

    void pridejZaznam(int kolikDni, int najetoKm, string jmeno){
        m_najeto += najetoKm;
        m_zisk = m_zisk + spocitejCenu(kolikDni);
        setJmeno(jmeno);
    }

    void printInfo(){
        cout << "Najeto: " << m_najeto << endl;
        cout << "Cena za den: " << m_cena << endl;
        cout << "Zisk: " << m_zisk << endl;
        cout << "Posledni majitel:" << m_posledniMajitel << endl;
    }

};

int main() {
    Auto* a1 = new Auto(0,100,0,"Mikulas");
    a1->pridejZaznam(1,11000,"M");
    cout << a1->spocitejCenu(1) << endl;
    a1->printInfo();
    return 0;
}