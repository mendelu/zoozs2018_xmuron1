//
// Created by xmuron1 on 15.11.2018.
//

#ifndef UIS_UIS_H
#define UIS_UIS_H

#include <iostream>
#include <vector>
#include "Student.h"
#include "Ucitel.h"

class UIS {
private:
    std::vector<Student*> m_studenti;
    std::vector<Ucitel*> m_ucitele;

public:
    void pridejStudenta(Student* novyStudent);
    void pridejStudenta(std::string jmeno, std::string rc, float prumer, int semestr);
    void pridejUcitele(Ucitel* ucitel);
    void vypisStudenty();
    void vypisUcitele();
    void vypisStudentyProStipendia();
    void vypisVsechny();

};


#endif //UIS_UIS_H
