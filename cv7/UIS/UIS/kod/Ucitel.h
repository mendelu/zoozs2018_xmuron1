//
// Created by Mikulas Muron on 15/11/2018.
//

#ifndef UIS_UCITEL_H
#define UIS_UCITEL_H

#include <iostream>
#include "Osoba.h"

class Ucitel : public Osoba {
private:
    std::string m_ustav;
public:
    Ucitel(std::string jmeno, std::string rc, std::string ustav);
    void setUstav(std::string ustav);
    std::string getUstav();
    void printInfo();
};


#endif //UIS_UCITEL_H
