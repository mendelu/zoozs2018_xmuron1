//
// Created by xmuron1 on 15.11.2018.
//

#ifndef UIS_OSOBA_H
#define UIS_OSOBA_H

#include <iostream>
class Osoba {
    std::string m_jmeno;
    std::string m_rc;
public:
    Osoba(std::string jmeno,std::string rc);
    std::string getJmeno();
    std::string getRC();
    void setRC(std::string rc);
    void setJmeno(std::string jmeno);
};


#endif //UIS_OSOBA_H
