//
// Created by Mikulas Muron on 15/11/2018.
//

#include "Ucitel.h"

Ucitel::Ucitel(std::string jmeno, std::string rc, std::string ustav): Osoba(jmeno,rc) {
    m_ustav = ustav;
}

void Ucitel::setUstav(std::string ustav) {
    m_ustav = ustav;
}

std::string Ucitel::getUstav() {
    return m_ustav;
}

void Ucitel::printInfo() {
    std::cout << "Jsem ucitel se jmenem " << getJmeno() << " a s rc " << getRC() << " pracuji na ustavu " << getUstav() << std::endl;
}