//
// Created by xmuron1 on 15.11.2018.
//

#ifndef UIS_STUDENT_H
#define UIS_STUDENT_H


#include <iostream>
#include "Osoba.h"

class Student : public Osoba {
private:
    float m_prumer;
    int m_semestr;
public:
    Student(std::string jmeno, std::string rc, float prumer, int semestr);
    int getSemestr();
    float getPrumer();
    void setPrumer(float prumer);
    void setSemestr(int semestr);
    void printInfo();
};


#endif //UIS_STUDENT_H
