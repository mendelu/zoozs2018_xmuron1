//
// Created by xmuron1 on 15.11.2018.
//

#include "Osoba.h"

Osoba::Osoba(std::string jmeno, std::string rc) {
    setRC(rc);
    setJmeno(jmeno);
}

std::string Osoba::getJmeno() {
    return m_jmeno;
}

std::string Osoba::getRC() {
    return m_rc;
}

void Osoba::setJmeno(std::string jmeno) {
    // Pridat kontrolu na jmeno
    m_jmeno = jmeno;
}

void Osoba::setRC(std::string rc) {
    // Pridat kontrolu na RC
    m_rc = rc;
}