//
// Created by xmuron1 on 15.11.2018.
//

#include "UIS.h"

void UIS::pridejStudenta(Student *novyStudent) {
    m_studenti.push_back(novyStudent);
}

void UIS::pridejStudenta(std::string jmeno, std::string rc, float prumer, int semestr) {
    Student* student = new Student(jmeno,rc,prumer,semestr);
    pridejStudenta(student);
}

void UIS::pridejUcitele(Ucitel* ucitel) {
    m_ucitele.push_back(ucitel);
}

void UIS::vypisStudenty() {
    for(Student* s : m_studenti){
        s->printInfo();
    }
}

void UIS::vypisStudentyProStipendia() {
    for(Student* s : m_studenti){
        if(s->getPrumer() < 2) {
            s->printInfo();
        }
    }
}

void UIS::vypisUcitele() {
    for(Ucitel* t : m_ucitele){
        t->printInfo();
    }
}

void UIS::vypisVsechny() {
    vypisUcitele();
    vypisStudenty();
}