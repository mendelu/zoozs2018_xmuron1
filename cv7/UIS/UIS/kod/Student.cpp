//
// Created by xmuron1 on 15.11.2018.
//

#include "Student.h"

Student::Student(std::string jmeno, std::string rc, float prumer, int semestr) : Osoba(jmeno,rc) {
    setPrumer(prumer);
    setSemestr(semestr);
}

void Student::setPrumer(float prumer) {
    if(prumer >= 0 && prumer <= 5) {
        m_prumer = prumer;
    }
    else{
        m_prumer = 0;
    }
}

void Student::setSemestr(int semestr) {
    if(semestr > 0 && semestr < 13) {
        m_semestr = semestr;
    }
    else{
        m_semestr = 0;
    }
}

float Student::getPrumer() {
    return m_prumer;
}

int Student::getSemestr() {
    return m_semestr;
}

void Student::printInfo() {
    std::cout << "Jsem student se jmenem " << getJmeno() << " a rc " << getRC() << " mam prumer " << getPrumer() << " a studuji v semestru cislo " << getSemestr() << std::endl;
}