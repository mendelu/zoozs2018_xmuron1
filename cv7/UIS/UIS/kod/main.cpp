#include <iostream>
#include "UIS.h"

int main() {
    UIS* uis = new UIS();
    Student* s1 = new Student("Mikulas","12345/1234",0,1);
    Ucitel* u1 = new Ucitel("David", "332/32","UI");

    uis->pridejStudenta(s1);
    uis->pridejUcitele(u1);

    uis->vypisStudenty();
    uis->vypisUcitele();


    return 0;
}