//
// Created by Mikulas Muron on 26/11/2018.
//

#include "Student.h"
#include "PrezencniStudent.h"
#include "KombinovanyStudent.h"

Student::Student(string jmeno, OborStudia obor, int studujeLet, int prumer) {
    m_jmeno = jmeno;
    m_obor = obor;
    m_studujeLet = studujeLet;
    m_studijniPrumer = prumer;
}

void Student::pridejKOdstudovanymLetum(int kolikLet) {
    if(kolikLet > 0) {
        m_studujeLet += kolikLet;
    }
}

void Student::setStudijniPrumer(float novyPrumer) {
    if(novyPrumer > 0 && novyPrumer <= 5){
        m_studijniPrumer = novyPrumer;
    }
    else{
        m_studijniPrumer = 3;
    }
}


string Student::getJmeno() {
    return m_jmeno;
}


OborStudia Student::getObor(){
    return m_obor;
}

int Student::getPocetLetStudia() {
    return m_studujeLet;
}

float Student::getPrumer() {
    return m_studijniPrumer;
}

Student* Student::getStudent(string jmeno, int dobaStudia, OborStudia obor, int dojezdovaVzdalenost) {
    int vychoziPrumer = 2;

    if(dobaStudia == PrezencniStudent::s_pocetLetStudia){
        return new PrezencniStudent(jmeno, obor, 0, vychoziPrumer);
    }
    else if(dobaStudia == KombinovanyStudent::s_pocetLetStudia){
        return new KombinovanyStudent(jmeno,obor,0,vychoziPrumer,dojezdovaVzdalenost);
    }
    else{
        std::cout << "Zadan spatny pocet let studia. Nemohu vytvorit pozadovaneho studenta" << std::endl;
        return nullptr;
    }
}