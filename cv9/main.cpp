#include <iostream>

#include "SeznamStudentu.h"

int main() {
    SeznamStudentu::pridej("Mikulas M",3,OborStudia::EI,2);
    SeznamStudentu::pridej("Frantisek A",5,OborStudia::ARI,150);
    Student* josef = Student::getStudent("Josef S",5,OborStudia::EI, 200);
    josef->pridejKOdstudovanymLetum(7);
    SeznamStudentu::pridej(josef);

    SeznamStudentu::printInfo();
    std::cout << "Celkove vyplaceno stipendii:" << SeznamStudentu::celkovaSumaStipendii() << std::endl;
    std::cout << "Prodluzuje studium: " << SeznamStudentu::kolikProdluzujeStudium() << std::endl;
    return 0;
}