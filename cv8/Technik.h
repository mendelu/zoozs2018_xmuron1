//
// Created by David Prochazka on 15/11/2018.
//

#ifndef COMPANY_TECHNIK_H
#define COMPANY_TECHNIK_H

#include "PracovniPozice.h"

class Technik: public PracovniPozice{
    static const int s_maxDniDovolene = 30;
    static const int s_platovyZaklad = 20000;
    static const int s_rocniBous = 20000;

public:
    // override neni nutne, pouze se tim zdruraznuje, ze prekryvame existujici metodu.
    // Pokud bychom se treba prepsali v nazvu, obdrzime chybu. Je to dobre.
    int getPlat(int stupenVzdelani, int letVeSpolecnosti) override;
    int getPoceDniDovoleni(int cerpanoDni) override;
};


#endif //COMPANY_TECHNIK_H
