//
// Created by David Prochazka on 15/11/2018.
//

#ifndef COMPANY_AKADEMIK_H
#define COMPANY_AKADEMIK_H


#include "PracovniPozice.h"

class Akademik: public PracovniPozice{
    static const int s_maxDniDovolene = 50; // lze dat do predka, ale je to konstanta, takze to nema smysl
    static const int s_platovyZaklad = 30000;
    static const int s_bonusVzdelani = 5000;

public:
    int getPlat(int stupenVzdelani, int letVeSpolecnosti) override;
    int getPoceDniDovoleni(int cerpanoDni) override;
};



#endif //COMPANY_AKADEMIK_H
