//
// Created by David Prochazka on 15/11/2018.
//

#ifndef COMPANY_PRACOVNIPOZICE_H
#define COMPANY_PRACOVNIPOZICE_H


class PracovniPozice {
public:
    // co musi poskytovat kazda pracovni pozice
    virtual int getPlat(int stupenVzdelani, int letVeSpolecnosti) = 0;
    virtual int getPoceDniDovoleni(int cerpanoDni) = 0;
};


#endif //COMPANY_PRACOVNIPOZICE_H
