//
// Created by David Prochazka on 15/11/2018.
//

#include "Akademik.h"

int Akademik::getPlat(int stupenVzdelani, int letVeSpolecnosti){
    return s_platovyZaklad + stupenVzdelani * s_bonusVzdelani;
}

int Akademik::getPoceDniDovoleni(int cerpanoDni){
    return s_maxDniDovolene - cerpanoDni;
}
