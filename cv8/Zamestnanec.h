//
// Created by David Prochazka on 15/11/2018.
//

#ifndef COMPANY_ZAMESTNANEC_H
#define COMPANY_ZAMESTNANEC_H

#include <iostream>
#include "Technik.h"
#include "Akademik.h"

enum class PoziceZamestnance{
    Akademik, Technik
};

class Zamestnanec {
    int m_letVeSpolecnosti;
    int m_dosazeneVzdelani;
    std::string m_jmeno;
    int m_cerpanoDniDovolene;

    PracovniPozice* m_pozice;

public:
    Zamestnanec(std::string jmeno, int letVeSpolecnosti, int dosazeneVzdelani, PoziceZamestnance pozice);

    // toto je specialni metoda, kterou potrebujeme pro "prepinani" pozice
    void zmenPoziciZamestnance(PoziceZamestnance novaPozice);

    // Toto jsou bezne metody pro tridu
    int getPlat();
    int getPocetDniDovolene();
    int evidujDovolenou(int dniDovolene);
};


#endif //COMPANY_ZAMESTNANEC_H
