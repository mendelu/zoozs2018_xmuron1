#include <iostream>

using namespace std;

class Prisera{
private:
    int m_zivoty;
public:
    Prisera(int zivoty){
        m_zivoty = zivoty;
    }

    int getZivot(){
        return m_zivoty;
    }

    void snizZivot(int kolikZivotu){
        m_zivoty = m_zivoty - kolikZivotu;
    }

    void printInfo(){
        cout << "Prisera ma: " << m_zivoty << " zivotu" << endl;
    }
};

class Hrdina{
private:
    string m_jmeno;
    int m_utok;
    int m_zivoty;
public:
    static int s_pocet;

    Hrdina(string jmeno, int utok, int zivot){
        m_jmeno = jmeno;
        m_utok = utok;
        m_zivoty = zivot;
        Hrdina::s_pocet = Hrdina::s_pocet+1;
    }

    ~Hrdina(){
        Hrdina::s_pocet = Hrdina::s_pocet-1;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void setJmeno(string noveJmeno){
        m_jmeno = noveJmeno;
    }

    int getUtok(){
        return m_utok;
    }

    static int pocetHrdinu(){
        return Hrdina::s_pocet;
    }

    void zautoc(Prisera* nepritel){
        if(nepritel->getZivot() > 0) {
            nepritel->snizZivot(getUtok());
        }
        else{
            cout << "Nelze utocit na mrtvou priseru!" << endl;
        }
    }
};

int Hrdina::s_pocet = 0;

int main() {
    Hrdina* h1 = new Hrdina("Artus",10,100);
    Hrdina* h2 = new Hrdina("Tom",20,100);
    Hrdina* h3 = new Hrdina("Eva",30,100);

    Prisera* p1 = new Prisera(100);

    h1->zautoc(p1);
    h2->zautoc(p1);
    h2->zautoc(p1);

    cout << "Moje druzina ma: " << Hrdina::pocetHrdinu() << " hrdinu." << endl;
    delete p1;
    delete h1;
    delete h2;
    delete h3;
    return 0;
}