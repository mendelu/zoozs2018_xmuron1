#include <iostream>

using namespace std;

class Student {
public:
    int m_rocnik;
    string m_jmeno;

    void setJmeno(string jmeno){
        this->m_jmeno = jmeno;
    }
    string getJmeno(){
        return this->m_jmeno;
    }
};

int main() {
    Student* mik = new Student();
    mik->setJmeno("Mikulas Muron");
    cout << mik->getJmeno() << endl;
}